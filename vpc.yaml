AWSTemplateFormatVersion: "2010-09-09"
Description: The infrastructure required for {{AppName}}Dev apps.
Parameters:
  CidrVpc:
    Description: CIDR for the VPC.
    Type: String
    Default: '10.0.0.0/24'
  CidrPublicSubnet1:
    Description: CIDR for the first public subnet. 
    Type: String
    Default: 10.0.0.0/26
  CidrPublicSubnet2:
    Type: String
    Default: '10.0.0.64/26'
    Description: CIDR for the second public subnet. 
  CidrPrivateSubnet1:
    Description: CIDR for the first private subnet. 
    Type: String
    Default: '10.0.0.128/26'
  CidrPrivateSubnet2:
    Description: CIDR for the second private subnet. 
    Type: String
    Default: '10.0.0.192/26'
  PublicSubnet1Az:
    Description: The AZ for public subnet 1 
    Type: String
    Default: us-east-1a
  PublicSubnet2Az:
    Description: The AZ for public subnet 2
    Type: String
    Default: us-east-1b
  PrivateSubnet1Az:
    Description: The AZ for private subnet 1 
    Type: String
    Default: us-east-1a
  PrivateSubnet2Az:
    Description: The AZ for private subnet 2
    Type: String
    Default: us-east-1b
  ParamOwnerIndividualTagValue:
    Type: String
    Default: 'rumesh@athukorala.com'
    Description: Resource Responsible Owner Email Address e.g. owner@athukorala.com
  ParamResponsibleIndividualsTagValue:
    Type: String
    Default: 'rumesh@athukorala.com'
    Description: Resource Responsible Individuals Email Distribution Group
      e.g. owner@athukorala.com
  ParamEnvironmentTagValue:
    Type: String
    Default: DEV
    Description: Environment resource belongs to. Value can be Development, QA, NFT,
      Development or Production
  ParamLCEnvironmentTagValue:
    Type: String
    Default: DEV
    Description: S3 friendly name Environment resource belongs to.
  ParamAwsconTagValue:
    Type: String
    Default: Development
    Description: Tag for utilizing reports

Resources:
  {{AppName}}DevVPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref CidrVpc
      EnableDnsSupport: true
      EnableDnsHostnames: true
      InstanceTenancy: default
      Tags:
        - Key: Name
          Value: {{AppName}}DevVPC
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevVPCInternetGW:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: {{AppName}}DevVPC-IGW
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevVPCAttachInternetGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
       VpcId: !Ref {{AppName}}DevVPC
       InternetGatewayId: !Ref {{AppName}}DevVPCInternetGW

  {{AppName}}DevVPCFlowLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: {{AppName}}DevVPC-LogGroup
      RetentionInDays: 365

  {{AppName}}DevVPCFlowLogRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Sid: AllowFlowLogServiceToAssumeRole
            Effect: Allow
            Principal:
              Service: vpc-flow-logs.amazonaws.com
            Action: sts:AssumeRole
      Path: /

  {{AppName}}DevVPCFlowLogPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      Description: "Policy for viewing account billing information in the AWS Portal"
      PolicyDocument:
          Version: 2012-10-17
          Statement:
            - Sid: AllowUsageofLogGroup
              Action:
                - logs:CreateLogGroup
                - logs:CreateLogStream
                - logs:PutLogEvents
                - logs:DescribeLogGroups
                - logs:DescribeLogStreams
              Effect: Allow
              Resource: "*"
      Roles:
        - !Ref {{AppName}}DevVPCFlowLogRole

  {{AppName}}DevVPCFlowLog:
    Type: AWS::EC2::FlowLog
    Properties:
      DeliverLogsPermissionArn: !GetAtt {{AppName}}DevVPCFlowLogRole.Arn
      LogGroupName: !Ref {{AppName}}DevVPCFlowLogGroup
      ResourceId: !Ref {{AppName}}DevVPC
      ResourceType: VPC
      TrafficType: ALL

  {{AppName}}DevVPCVPCDefaultSecurityGroupEgress:
    Type: AWS::EC2::SecurityGroupEgress
    Properties:
      GroupId: !GetAtt {{AppName}}DevVPC.DefaultSecurityGroup
      IpProtocol: -1
      CidrIp: 127.0.0.1/32

  {{AppName}}DevVPNGateway:
    Type: AWS::EC2::VPNGateway
    Properties:
      Type: ipsec.1
      AmazonSideAsn: {{ASN}}
      Tags:
        - Key: Name
          Value: {{AppName}}dev_VPNGateway
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevVPNVPCGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties: 
      VpcId: !Ref {{AppName}}DevVPC
      VpnGatewayId: !Ref {{AppName}}DevVPNGateway

  {{AppName}}DevPublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Join ['', [!Ref 'AWS::Region', a]]
      VpcId: !Ref {{AppName}}DevVPC
      MapPublicIpOnLaunch: true
      CidrBlock: !Ref CidrPublicSubnet1
      Tags:
        - Key: Name
          Value: !Join ['-', [public, !Ref PublicSubnet1Az, !Ref CidrPublicSubnet1]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPublicSubnet1RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref {{AppName}}DevVPC
      Tags:
        - Key: Name
          Value: !Join ['-', [public, !Ref PublicSubnet1Az, !Ref CidrPublicSubnet1]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue
  
  {{AppName}}DevPublicSubnet1RouteTableAssociation:
     Type: AWS::EC2::SubnetRouteTableAssociation
     Properties:
       SubnetId: !Ref {{AppName}}DevPublicSubnet1
       RouteTableId: !Ref {{AppName}}DevPublicSubnet1RouteTable

  {{AppName}}DevPublicSubnet1InternetGWRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref {{AppName}}DevPublicSubnet1RouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref {{AppName}}DevVPCInternetGW

  {{AppName}}DevPublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Join ['', [!Ref 'AWS::Region', b]]
      VpcId: !Ref {{AppName}}DevVPC
      MapPublicIpOnLaunch: true
      CidrBlock: !Ref CidrPublicSubnet2
      Tags:
        - Key: Name
          Value: !Join ['-', [public, !Ref PublicSubnet2Az, !Ref CidrPublicSubnet2]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPublicSubnet2RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref {{AppName}}DevVPC
      Tags:
        - Key: Name
          Value: !Join ['-', [public, !Ref PublicSubnet2Az, !Ref CidrPublicSubnet2]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPublicSubnet2RouteTableAssociation:
     Type: AWS::EC2::SubnetRouteTableAssociation
     Properties:
       SubnetId: !Ref {{AppName}}DevPublicSubnet2
       RouteTableId: !Ref {{AppName}}DevPublicSubnet2RouteTable

  {{AppName}}DevPublicSubnet2InternetGWRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref {{AppName}}DevPublicSubnet2RouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref {{AppName}}DevVPCInternetGW

  {{AppName}}DevPrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Join ['', [!Ref 'AWS::Region', a]]
      VpcId: !Ref {{AppName}}DevVPC
      MapPublicIpOnLaunch: false
      CidrBlock: !Ref CidrPrivateSubnet1
      Tags:
        - Key: Name
          Value: !Join ['-', [private, !Ref PrivateSubnet1Az, !Ref CidrPrivateSubnet1]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPrivateSubnet1RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
       VpcId: !Ref {{AppName}}DevVPC
       Tags:
        - Key: Name
          Value: !Join ['-', [private, !Ref PrivateSubnet1Az, !Ref CidrPrivateSubnet1]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPrivateSubnet1RouteTableAssociation:
     Type: AWS::EC2::SubnetRouteTableAssociation
     Properties:
        SubnetId: !Ref {{AppName}}DevPrivateSubnet1
        RouteTableId: !Ref {{AppName}}DevPrivateSubnet1RouteTable

  {{AppName}}DevPrivateSubnet1NATGW:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt {{AppName}}DevPrivateSubnet1EIP.AllocationId
      SubnetId: !Ref {{AppName}}DevPublicSubnet1
      Tags:
        - Key: Name
          Value: !Join ['-', [NATGW, !Ref PrivateSubnet1Az, !Ref CidrPrivateSubnet1]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPrivateSubnet1NATGWRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref {{AppName}}DevPrivateSubnet1RouteTable
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref {{AppName}}DevPrivateSubnet1NATGW

  {{AppName}}DevPrivateSubnet1EIP:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc

  {{AppName}}DevPrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Join ['', [!Ref 'AWS::Region', b]]
      VpcId: !Ref {{AppName}}DevVPC
      MapPublicIpOnLaunch: false
      CidrBlock: !Ref CidrPrivateSubnet2
      Tags:
        - Key: Name
          Value: !Join ['-', [private, !Ref PrivateSubnet2Az, !Ref CidrPrivateSubnet2]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPrivateSubnet2RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
       VpcId: !Ref {{AppName}}DevVPC
       Tags:
        - Key: Name
          Value: !Join ['-', [private, !Ref PrivateSubnet2Az, !Ref CidrPrivateSubnet2]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue

  {{AppName}}DevPrivateSubnet2RouteTableAssociation:
     Type: AWS::EC2::SubnetRouteTableAssociation
     Properties:
        SubnetId: !Ref {{AppName}}DevPrivateSubnet2
        RouteTableId: !Ref {{AppName}}DevPrivateSubnet2RouteTable

  {{AppName}}DevPrivateSubnet2NATGW:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt {{AppName}}DevPrivateSubnet2EIP.AllocationId
      SubnetId: !Ref {{AppName}}DevPublicSubnet2
      Tags:
        - Key: Name
          Value: !Join ['-', [NATGW, !Ref PrivateSubnet2Az, !Ref CidrPrivateSubnet2]]
        - Key: t_owner_individual
          Value: !Ref ParamOwnerIndividualTagValue


  {{AppName}}DevPrivateSubnet2NATGWRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref {{AppName}}DevPrivateSubnet2RouteTable
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref {{AppName}}DevPrivateSubnet2NATGW

  {{AppName}}DevPrivateSubnet2EIP:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc        

  {{AppName}}DevPublicSubnet1RouteTableEnablePropergation:
    Type: AWS::EC2::VPNGatewayRoutePropagation
    Properties: 
      RouteTableIds: 
        - !Ref {{AppName}}DevPublicSubnet1RouteTable
      VpnGatewayId: !Ref {{AppName}}DevVPNGateway
    DependsOn: {{AppName}}DevVPCAttachInternetGateway

  {{AppName}}DevPublicSubnet2RouteTableEnablePropergation:
    Type: AWS::EC2::VPNGatewayRoutePropagation
    Properties: 
      RouteTableIds: 
        - !Ref {{AppName}}DevPublicSubnet2RouteTable
      VpnGatewayId: !Ref {{AppName}}DevVPNGateway
    DependsOn: {{AppName}}DevVPCAttachInternetGateway

  {{AppName}}DevPrivateSubnet1RouteTableEnablePropergation:
    Type: AWS::EC2::VPNGatewayRoutePropagation
    Properties: 
      RouteTableIds: 
        - !Ref {{AppName}}DevPrivateSubnet1RouteTable
      VpnGatewayId: !Ref {{AppName}}DevVPNGateway
    DependsOn: {{AppName}}DevVPNVPCGatewayAttachment

  {{AppName}}DevPrivateSubnet2RouteTableEnablePropergation:
    Type: AWS::EC2::VPNGatewayRoutePropagation
    Properties: 
      RouteTableIds: 
        - !Ref {{AppName}}DevPrivateSubnet2RouteTable
      VpnGatewayId: !Ref {{AppName}}DevVPNGateway
    DependsOn: {{AppName}}DevVPNVPCGatewayAttachment
  
Outputs:
  {{AppName}}DevVPC:
    Description: The VPC id for {{AppName}}Dev
    Value: !Ref {{AppName}}DevVPC
    Export:
        Name: {{AppName}}DevVPC

  {{AppName}}DevPublicSubnet1:
    Description: The PublicSubnet1 id for {{AppName}}Dev
    Value: !Ref {{AppName}}DevPublicSubnet1
    Export:
        Name: {{AppName}}DevPublicSubnet1

  {{AppName}}DevPublicSubnet2:
    Description: The PublicSubnet2 id for {{AppName}}Dev
    Value: !Ref {{AppName}}DevPublicSubnet2
    Export:
        Name: {{AppName}}DevPublicSubnet2

  {{AppName}}DevPrivateSubnet1:
    Description: The PrivateSubnet1 id for {{AppName}}Dev
    Value: !Ref {{AppName}}DevPrivateSubnet1
    Export:
        Name: {{AppName}}DevPrivateSubnet1

  {{AppName}}DevPrivateSubnet2:
    Description: The PrivateSubnet2 id for {{AppName}}Dev
    Value: !Ref {{AppName}}DevPrivateSubnet2
    Export:
        Name: {{AppName}}DevPrivateSubnet2
